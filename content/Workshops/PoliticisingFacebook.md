# Politicising Facebook
--- meta
title: Politicising Facebook
uuid: a79790b4-9ef0-45fb-8230-1843ceba686b
lan: en
source: Tactical Tech
item: SLE
tags:
  - Social media
  - Debate  
duration: 90
description: A critical, discussion-based look at Facebook from a social and political perspective.
---

## Meta information

### Description
A critical, discussion-based look at Facebook from a social and political perspective.  

### Overview
1. Introductions (10 min)
2. \\\[optional\\\] Crowd-source statements about Facebook (10 min)
3. First Lager House Debate (30 min)
4. \\\[optional\\\] Second Lager House Debate (30 min)
5.  Wrap up (10 min)

### Duration
90 minutes.

### Ideal Number of Participants
Minimum of 8.

### Workshop Objectives
##### Knowledge
- Understand central social, cultural and political issues around Facebook

##### Skill
- Be able to construct and use logical arguments in discussion

##### Attitude
- Social Media is occupying more and more space in the political sphere and public debate. We tend to forget that those platform are privately owned and are subject to the rules, control and interests of the corporations that own them.

### References
- [Facebook experiment in social influence and political mobilization](http://journalistsresource.org/studies/politics/digital-democracy/facebook-61-million-person-experiment-social-influence-political-mobilization) (Journalist's Resource)
- [A 61-million-person experiment in social influence and political mobilization](http://www.nature.com/nature/journal/v489/n7415/full/nature11421.html) (Nature International Journal of Science)

### Materials and Equipment Needed
- @[material](c0358b51-fe16-47ae-9686-927ec39d18f6)
- Chairs to sit on (one per participant), or if no chairs available, tape to put on the floor and divide the space.


## Steps

### Preparation
The workshop requires participants to debate political statements about Facebook.
1. *Option A* requires you to provide the group with statements.
2. *Option B* involves crowd-sourcing statements from the participants. If you take this option, you should however still prepare some statements as back-up - ideally, contextualised to the group or region.  

##### Example statements
1. Facebook has compelling reasons to censor - to limit profanity and hate speech.
2. Facebook should be the one setting the rules for what happens on the platform; not users or the government.
3. Facebook enables violence towards women who express their opinions.  

### Step 1: Introductions (10 min)
1. Briefly introduce yourself and the session, then ask participants to introduce themselves and say what they hope to learn in this session.
2. Taking expectations into account, give a brief overview, including objectives, what will be covered (and what not), and how much time is available.

### Step 2 \\\[optional\\\] Crowd-source statements about Facebook (10 min)
1. Break the group into smaller groups
2. Ask each group to write down, on post-its, as many statements about Facebook that they can think of (One statement per post-it). For context, ask them to think about the centralisation of power and data, living in a "Post-Privacy world", etc.
3. Each group should then choose one statement.
4. Bring the groups back together, and ask each group to read out their statement. These will be used for the following activity.


### Step 3: First Lager House Debate (30 min)
Use the selected post-it statements from Step 2, or use your back-up statements, to hold a lager-house discussion.


##### Activity: "Lager House Discussion"
@[activity](6d8b622b-de50-4dc6-8696-1b2bfde78dec)

### Step 4: \\\[optional\\\] Second Lager House Debate (30 min)
_This step can be used as either an alternative to, or a follow on from, the previous debate._

1. Use a Lager House format (as laid out above) to have the group discuss the following statement: "Facebook is a Public Space".

2. In the follow-up discussion, ask the group some of the following:
    - Are social media platforms public or private spaces? How does the general public view them?
    - Who has control over Facebook and what messages are we are allowed to deliver?
    - What is an algorithm? How do algorithms affect the public debate? Any famous cases?  
    - Does censorship have a legitimate place in how Facebook operates? Collaboration with governments?
    - Collection of data is a concern since extensive profiles can be drawn from the data shared on Facebook. Who has access to this data? Do we, or should we, trust a corporation with that?
    - When we become very dependent on Facebook to reach an audience, or to connect with our friends, what does this kind of centralisation of power mean for us? What happens when Facebook decides to close our account, or if Facebook is blocked from our country?
    - In some countries, Facebook *is* - or is trying to be - the internet. What does that mean for the people living in those countries?

### Step 5: Wrap up (10 min)
1. See if anything is still unclear, and answer questions
2. Direct participants to resources

-------------------------------
<!---
data-privacy-training/SLEs/TEMPLATE
-->
