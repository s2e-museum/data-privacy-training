# Strategies of Resistance
--- meta
title: Strategies of Resistance
uuid: fc709e80-4eca-4185-927a-7ea3b0d13659
lan: en
source: Tactical Tech
item: Activity
tags:
  - Mobile
  - Social media
  - Secure messaging
  - Browser
  - Choosing tools
  - Collecting ideas
duration: 60
description: Four broad strategies for messing around with your digital shadow.
---

## Meta Information

### Description
Four broad strategies for messing around with your digital shadow.

### Duration
60 minutes.

### Ideal Number of Participants
Minimum of 9 participants.


### Learning Objectives
##### Knowledge
- Gain insight into different strategies for playing with your data.
- Understand the benefits and limitations of each strategy.

##### Skills
- Be able to use some of the strategies discussed.


### Materials and Equipment Needed
- @[material](417fc8a5-400f-4553-a23e-faa949beb239)
- @[material](9392dacf-999c-4c33-a6d8-4545c1aee849)
- @[material](884b8427-f8ba-441c-a663-b32282aaeb41)


## Activity: "Strategies of Resistance"
--- meta
uuid: 1667798e-fb3d-4ae6-a744-ddc6ae44bdf8
---

#### Preparation
1. Prepare to present the four categories of resistance. Have a full list of examples for each one.
2. Decide which area you would like the group to focus on (for example browser tracking, location tracking, mobile phones in general, etc).

#### Four types of resistance (15 min)
1. Ask participants for a few ways they have tried to increase their privacy online. Put some of these on the board.
2. Use the examples on the board to lay out four broad strategies of resistance:
    - Reduction (Reduce)
    - Obfuscation (Confuse/Create noise)
    - Compartimentalisation (Separate)
    - Fortification (Fortify)

#### Brainstorm strategies  (20 min)
1. Split participants into four groups, and give each group one of the four strategies: Reduction, Obfuscation, Compartmentalisation, Fortification.
2. Set the focus area.
3. Each group should brainstorm ways in which they can reduce / obfuscate / compartmentalise /fortify their data in this area.

#### Feedback: presentations  (15 min)
Each group reports back to the entire group in a 2-3 minute presentation.

#### Discussion (10 min)
Lead a brief discussion on the benefits and limitations of each strategy, feeding in where necessary.


-------------------------------
<!---
data-privacy-training/Activities/TEMPLATE
-->
