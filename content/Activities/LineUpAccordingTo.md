# Line up according to...
--- meta
title: Line up according to...
uuid: 6e146239-0503-4ed2-97b4-ea1e948f4591
lan: en
source: Tactical Tech
item: Activity
tags:
  - Mobile
  - Social media
  - Secure messaging
  - Browser
  - Choosing tools
  - Collecting ideas
duration: 10
description: Get everyone thinking about their own relationship with a specific tool or service.
---

## Meta information

### Description
Get everyone thinking about their own relationship with a specific tool or service.

### Duration
10 minutes.

### Ideal Number of Participants
Minimum of 4.

## Activity: "Line Up According to...."
--- meta
uuid: 9603b159-bf52-428b-a8a0-902486d2205e
---

#### Preparation
1. Prepare at least six statements related to participants' relationship with a specific tool or service. Each statement should correspond to a behaviour that the workshop is designed to correct, eg:
    - "Everyone who has used the same password for different accounts."
    - "Everyone who does not use 2-factor authentification on their main email account".
    - "Everyone who uses (service X)"
    - "Everyone who uses (service X) on their phone"
2. Each statement should be given a weight: one or two steps forward.
3. Prepare around 4 relatively banal statements to use in the demo. They can be fun, but be careful to make sure there's nothing that's too personal or revealing, or that could make participants feel uncomfortable.

#### Demo
1. Get participants to line up against the wall.
2. Introduce the activity and read out your first 'demo' statement, e.g. "Everyone who did not have 8 hours sleep last night, take one step forward." Go through the rest of the demo statements.

#### Get Started
1. Read out the first statement. Those who agree with it should take a step forward.
2. Read out the next statements one by one.
3. Once you've gone through all these statement, ask the group: Why are these questions important? What do they have to do with data privacy?
4. You don't want the activity to turn into an exercise in 'shaming'. Making the framing quite open (e.g. 'if you have ever done x' instead of 'if you _do_ x'), and participating yourself, can help to avoid this.


-------------------------------
<!---
data-privacy-training/Activities/TEMPLATE
-->
