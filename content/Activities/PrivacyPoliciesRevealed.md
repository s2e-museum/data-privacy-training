# Privacy Policies Revealed
--- meta
title: Privacy Policies Revealed
uuid: 78aeffb5-8eae-4cc4-975f-dfcd89d287de
lan: en
source: Tactical Tech
item: Activity
tags:
  - Mobile
  - Social media
  - Secure messaging
  - Browser
  - Choosing tools
duration: 30
description: Privacy Policies don't have to be so intimidating.
---

## Meta information

### Description
Privacy Policies don't have to be so intimidating.

### Duration
30 minutes.


### Ideal Number of Participants
This activity can be done with any number of participants.


### Learning Objectives
##### Knowledge
- Know more about the data use of some popular apps and tools.
- Gain more awareness on what kind of thing gets lost in small print.
- Understand why terms of service and privacy policies matter.

##### Skills
- Know where to find analyses of Terms of Service and Privacy Policies

### References
- [Lost in Small Print](https://myshadow.org/lost-in-small-print) (Tactical Tech)
- [Terms of Service - didn't read](https://tosdr.org/) - _Note - this website is currently out of date, but still useful for demonstration purposes._

### Materials and Equipment Needed
- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)


## Activity: "Privacy Policies Revealed"
--- meta
uuid: f690c52d-b314-4f05-bead-47a5dc141c78
---
#### Preparation
1. Have a look through both Tactical Tech's _Lost in Small Print_, and the Terms of Service Didn't Read website, and choose which specific service or services you'd like to focus on.
2. Have the two websites open and ready to be presented.  
3. For the company you're focusing on, make a list of some of the different products/services they offer, and list the data traces connected with that (ie, what data are these services getting from you?)

#### Introduce Terms of Service documents (15 min)
1. Ask the group if anyone has ever read a Terms of Service document. Explain what this is, if necessary.
2. Demonstrate [Lost in Small Print](https://myshadow.org/lost-in-small-print) Present and read out the important parts of the privacy policy.
3. Show the [Terms of Service; Didn't Read](https://tosdr.org) website, noting that the information on the website is not current!
4. If you don't have a laptop/projector/internet connection, print out copies of the relevant privacy policies and manually highlight the parts highlighted by _Lost in Small Print_.

#### Discussion (15 min)
Have a group discussion:
1. Was there anything in the previous demonstrations that surprised anyone?
2. What aspects of the Terms of Service are the most important?
